# Documentation Folder

You can find:

**Refer to `devdoc.md`**

if you want to know :
- How the **Classifier** will be trained.
- How the **DataSet** will be separated and preprocessed.
- How the **Converter** will be worked and what are the Optimizations methods that will be used.
- How the   **lite Model on coral board** will be worked.

## follow please the following sequence:
  
- read carefully devdoc
- dont use pleas the modules architecture to run the Project , it is now under developing and refactoring
- it is highly recommended to use Jupiter Notebooks to run the  `Training` Procedure  and `Evaluation and Conversion` Procedure   

