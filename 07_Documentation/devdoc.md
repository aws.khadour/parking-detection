# Developer Documentation

This document contains information on the architecture of the Parking place Classifier software


# Usage of the tool

# Requirements

Parking place Classifier requires `python3` and `pipenv` and `tenserflow > 2.0` and `CV2 library` and `edgetpu_compiler to be installed on Linux` to be installed. Additionally the necessary Libraries like `numby` and `panda`

Coral board `GOOGLE EDGE TPU with python3` to run the Model and predict the classes.



## Configuration

**config.json**
This file stores all the global parameters that can be used to configure Parking-detector and when this file leer is the default Configuration will be used in `configuration.py`


### (1) Preprocessing

- the images must be cropped to size  `[75*75]`
- image Name will begin with recorded Date `2021-03-01T08...`
- the Images will be trained depending on the Date pattern 
 - for example : the Validation Data-set is selected to be between `2021-03-03 00:00` and `2021-03-05 00:00`
 - Test Data-set is between  `2021-03-01 00:00` and `2021-03-03 00:00`
 - Training Data-set is every Date that is outside the mentioned last
 
 **dataPreprocessing.py**
 
 this script contain all the Methods that are necessary to this step
 
### the important Functions for dataPreprocessing API

|         Function | Purpose                                                                                                         |
|-----------------:|:----------------------------------------------------------------------------------------------------------------|
|   increase_brightness   | increase image-brightness                                                                                |
|   augment               | to augment the number of Images in train Data-set                                                        |                             
|   read_labels           | read labels files that contain input images with ground truth outputs                                    |                                                                  |
|   read_images_for_labels| read images from label file                                                                              |
|   train_test_split      | split the complete data-set to train, test, Validation Data-set                                          |
|   get_validation_x_and_y| get only the Validation Data-set                                                                         |



## powerTraining(2)
- perform Training for Parking-place Classifier , but with the respective preprocessed images
- the base models that have been selected are `DenseNet121, Inceptonv3, MobileNetV2, DenseNet169, ResNet152V2, ResNet101, Xception, InceptionResNetv2, Mobilenetsmall`
- 2 units at Output Layer are added to  classify empty or occupied state for every input image
- **important note** maybe you will see in Labels File Unknown state for Parking place , this state will be removed and not be considered in the Classification
- the methodology of the Training is `Fine-tuning` , top Layer for the Classification in the base Model will be freezed , but the initials values of the Weights will be used from the last Training of the base Model on the ImageNet Data-set
- that means the Learning is End-to-End Learning with the new two Units
- is used also many different Ways to select the best hyperparameters like `Grid search, Bayes search`


### the important Functions for PowerTraining API

|         Function | Purpose                                                                                                         |
|-----------------:|:----------------------------------------------------------------------------------------------------------------|
|   generate_file_name_of_model | generate file Name of the Model depending on `hyperparameters, ModelName`                          |
|   build_model                 | create Tenserflow Model based on some Configuration from  `Configuration.py`                       |                             
|   train_and_eval              | train and evaluate the Model                                                                       |                                                                 
|   train_eval_and_write_results| create   `result.csv` file to store the training result and is used to call `train_and_eval`function|                                                                    
|   train_test_split            | split the complete data-set to train, test, Validation Data-set                                    |



## Conversion(3)

- it will be simply summarized as following :
 - firstly we need to ensure that your `tenserflow version  > 2.0`, this step it is necessary to get `.tflite` format that happen by using the following functions:
  -  `tf.lite.TFLiteConverter.from_saved_model()` it is recommended from tenserflow website ,we should pass to this Function `savedformat.pb`
  -  `tf.lite.TFLiteConverter.from_keras_model` this function is used in parking-place lite Classifier , we should pass to this Function `keras-model`
  -  `tf.lite.TFLiteConverter.from_concrete_functions()`
  - this step is usually  integrated with another operation  `Optimization or Quantization ` that is necessary for Edge Devices like `moble,Microcontroller, Linux Embeded devices (pi, coral board)`
  - the main Goal of the `Optimization or Quantization` is to consider the Constraints of the Edg-devices like `Power-consumption, Latencey, limited  computing capabilities, limited Ram resources`
  - depending on the Edge-device ,the Strategy of  `Optimization or Quantization` will be selected
  - for the Edge-devices that work only with integer like `moble,Microcontroller, Linux Embeded devices (pi, coral board)` the `Post-training integer quantization or full integer quantization` must be used
  - the main Feature for `Post-training integer quantization or full integer quantization` is reducing the size and Latency of The Model 
  - the main disadvantage of `Post-training integer quantization or full integer quantization` is reducing the accuracy of the Model as a result that all Input and Outputs and weights are now only `int8,uint8`
  -`Post-training integer quantization or full integer quantization` is used till now for the Experiments
  -`Post-training integer quantization with int16 activations` will be also tested
 - second step is to create the Interpreter to run the tenserflow lite Model
 - third step is to compile the `full integer Quantization model version` using this tool `edgetpu_compiler` on Linux 
  - the Output file from `edgetpu_compiler` has the same name with this word `_edgetpu`
  
  
  
  
  
### the important Functions for converter API

|         Function |             Purpose                                                                                                         
|-----------------:|:----------------------------------------------------------------------------------------------------------------|
| representative_dataset_gen   | this is used to determine or estimate the Max and Min Value for Tensors(Input,weights, activation-outputs, Outputs) in the Model |                        
| convert_and_save_model_lite  | this is used to make dynamic Quantization and get lite model and save the converted model                                        |                             
| convert_and_quantize_and_save_mode_lite | this is used to make full integer  Quantization and get lite model and save the converted model                       |                                                  |                                                                 
| create_interpreter           | create interpreter to inference the Model                                                                                        | 




### the important Functions for evaluate API

|         Function |Purpose                                                                                                         
|-----------------:                    |:----------------------------------------------------------------------------------------------------------------|
| evaluate_tenserflow_model            | this Method is used to evaluate Model with Validation data                                                      | 
| evaluate_model_lite                  | this Method is used to evaluate Model with Validation data                                                      |                             
| calculate_accuracy_for_lite_model    | this Method us used to calculate accuracy depending on predicted classes from "evaluate_model_lite" Method      | 
                                                                                                  






 
    
 **important note** it is the same when we run the Model after full integer Quantization model version using the Interpreter on The Pc and the inference on the Coral-board
  the different only that coral-board need Compiled code from the Quantization model version
  
                                                                 

## Run the model on the Edge TPU (3)
- please be sure that you have followed all the last steps and you have all the Libraries 
- we assume that you have `coral-board with Mendel Linux` if not please check **https://coral.ai/docs/dev-board/get-started/#requirements**
- Clone this Git repo onto your computer:
   **mkdir google-coral && cd google-coral**
   ----------------------------------------
   **git clone https://github.com/google-coral/tflite**
   
- Install this example's dependencies:
  **cd tflite/python/examples/classification
  ./install_requirements.sh**
  
- use the `classification.py` script that should be in the Coral board operating system ,it should pass 3 arguments to this script using Terminal
- first argument `full integer Quantization and Compiled with edgetpu word model version `
- second argument `text file that contain name of the classes like empty , Occuiped ` when you do not use this file the result will be as Numbers that represent the Classes for example 0 for empty , 1 for occupied 
- third argument is the Name of the Image that we want to predict `xxx.jpg`




# Architecture of The repository

|           Foldername | Purpose                                                                                                                                                                                                                |
|---------------------:|:------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------           |
| 00_Administration    | Folder contains meeting Information                                                                                                                                                                                    |
| 01_literature_Review | Folder contains Literature study                                                                                                                                                                                       |
| 02_Requirments       | Folder contains required files for the Project                                                                                                                                                                         |
| 03_concepts          | Folder contains the Software Architecture of the Project                                                                                                                                                               |
| 04_Tests             | Folder the Experiments                                                                                                                                                                                                 |
| 05_Presentation      | Folder contains all presentations during the semester                                                                                                                                                                  |
| 06_implementation    | Folder contains the new realisation that consist of multiple Models (under development and Refactoring) and now is not recommended to use it, also contain Jupiters Notebooks that is highly now is recommended to use |                                                                                                                                                    |
| 08_mdoels            | Folder contains Standard models that is already trained                                                                                                                                                           |
| 09_lite_models       | Folder contains the lite Models                                                                                                                                                               |




# Files

|         Filename     | Purpose                                                                                                         |
|-----------------:    |:--------------------------------------------------------------------------------------------------------------- |
| powerTraining.py     | Python module that offers Power Training and Evaluation                                                         |
| configuration.py     | Python module that offers automatic reading of the config file and if not found returns a default configuration |
| dataPreprocessing.py | Python module that offers methods to process and read the                                                       |
| evaluate.py          | Python module that offers methods evaluate Lite and Standard models                                             |
| main.py              | Python module that contain the main Execution 

# Folders

|           Foldername | Purpose                                                                                                                                                                                                     |
|---------------------:|:------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
|         debug_images | Some images for debugging the processor.                                                                                                                                                                    |
|              results | Folder that will contain the results of an analysis for Standard Models                                                                                                                                     |                


**"important Note"** this devdoc is not final Documentation that why may be  it contains many wrongs in writing