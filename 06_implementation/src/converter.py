import numpy as np
import tensorflow as tf
from dataPreprocessing import get_validation_x_and_y
from  configration import get

# get the Json  configration  file
config = get()
image_path = config["IMAGE_DIR_PATH"]
min_width_of_images_to_read = config["MIN_WIDTH_OF_IMAGES_TO_READ"]
x_val_square, y_val_square = get_validation_x_and_y(image_path,min_width_of_images_to_read,config)


# this Method is used to generate representative_dataset
# representative_dataset is necessary for post-training-quantization
# this is used to determine or estimate the Max and Min Value for Tensors(Input,weights, activation-outputs, Outputs) in the Model
# the Numbers of Data is Randomly and depends on the Experiments
# it is necessary to be the Data float-32 !!!!


def representative_dataset_gen():
    representative_dataset = tf.data.Dataset.from_tensor_slices(x_val_square.astype('float32'))
    representative_dataset = representative_dataset.shuffle(buffer_size=10000).batch(1)

    for i, samples in enumerate(representative_dataset.take(100)):
        yield [samples]


# dynamic Quantization :
# only weights are converted to Integers with 8 bits
# Input and Output remain float-32 bit and activations als remain with float-32
# this Mode not suitable for only Integers Devices like Microcontroller and EDGE-TPU Devices
# this Mode does not require  Unlabeled Data-set or representative_dataset

def convert_and_save_model_lite(model):
    model = tf.keras.models.load_model(model)
    converter = tf.lite.TFLiteConverter.from_keras_model(model)
    converter.optimizations = [tf.lite.Optimize.DEFAULT]
    tflite_model = converter.convert()
    with open(config["MODEL_LITE_NAME"], 'wb') as f:
        f.write(tflite_model)
    return tflite_model


# post Training Quantization:
# full Integer or Uinteger Conversion
# Inputs and Outputs and Weights are Integer with 8 bit
# this Mode is preferable for speed Inference and small Latency
# this Mode is common for Microcontroller and EDGE-TPU Devices
# this Mode require Unlabeled Data-set or representative_dataset

def convert_and_quantize_and_save_mode_lite(model):
    converter = tf.lite.TFLiteConverter.from_keras_model(model)
    converter.optimizations = [tf.lite.Optimize.DEFAULT]
    converter.representative_dataset = representative_dataset_gen
    converter.target_spec.supported_ops = [tf.lite.OpsSet.TFLITE_BUILTINS_INT8]
    converter.inference_input_type = tf.uint8  # or tf.uint8
    converter.inference_output_type = tf.uint8  # or tf.uint8
    tflite_quant_model = converter.convert()
    with open(config["MODEL_LITE_NAME_QUANTIZED"], 'wb') as f:
            f.write(tflite_quant_model)
    return tflite_quant_model


# this Method is used to create Interpreter to run (inference) the Tenser flow Lite Model

def create_interpreter():
    interpreter = tf.lite.Interpreter(config["MODEL_LITE_NAME_QUANTIZED"])
    interpreter.allocate_tensors()
    input_type = interpreter.get_input_details()[0]['dtype']
    print('input: ', input_type)
    output_type = interpreter.get_output_details()[0]['dtype']
    print('output: ', output_type)

    return interpreter



