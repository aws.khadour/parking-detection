import os
from datetime import datetime
from os.path import abspath, join, dirname

import cv2
import numpy as np
import pandas as pd
import sklearn.metrics
import tensorflow as tf
from configration import get
from dataPreprocessing import train_test_split, append_row_to_eval_df, increase_brightness, augment

# get the Json  configration  file
config = get()


def generate_file_name_of_model(num_augmentations, use_tta, batch_size, image_size):
    base_model_type = config["BASE_MODEL_TYPE"]
    label_stlyle = config["LABEL_STYLE"]

    if num_augmentations > 0 and config["MIN_BRIGHTNESS_INCREASE"] == 0 and config["MAX_BRIGHTNESS_INCREASE"] == 0 and \
            config["MIN_AUG_SHIFT"] == 0 and config["MAX_AUG_SHIFT"] == 0 and not config["USE_LR_FLIP"]:
        augmentation_types = "onlymixup_"
    else:
        augmentation_types = "_"
    return f"{base_model_type}_{image_size}size_{batch_size}batch_{num_augmentations}aug_{int(use_tta)}tta_" + \
           f"{augmentation_types}{label_stlyle}.h5"


def build_model(base_model_type, image_size):
    if base_model_type == "DenseNet121":
        base_model = tf.keras.applications.DenseNet121(include_top=False, weights="imagenet", pooling="avg",
                                                       input_shape=(image_size[0], image_size[1], 3))
    elif base_model_type == "InceptionResNetV2":
        base_model = tf.keras.applications.InceptionResNetV2(include_top=False, weights="imagenet", pooling="avg",
                                                             input_shape=(image_size[0], image_size[1], 3))
    elif base_model_type == "Xception":
        base_model = tf.keras.applications.Xception(include_top=False, weights="imagenet", pooling="avg",
                                                    input_shape=(image_size[0], image_size[1], 3))
    elif base_model_type == "MobileNetV2":
        base_model = tf.keras.applications.MobileNetV2(include_top=False, weights="imagenet", pooling="avg",
                                                       input_shape=(image_size[0], image_size[1], 3))
    elif base_model_type == "DenseNet169":
        base_model = tf.keras.applications.DenseNet169(include_top=False, weights="imagenet", pooling="avg",
                                                       input_shape=(image_size[0], image_size[1], 3))
    elif base_model_type == "ResNet101V2":
        base_model = tf.keras.applications.ResNet101V2(include_top=False, weights="imagenet", pooling="avg",
                                                       input_shape=(image_size[0], image_size[1], 3))
    elif base_model_type == "ResNet152V2":
        base_model = tf.keras.applications.ResNet152V2(include_top=False, weights="imagenet", pooling="avg",
                                                       input_shape=(image_size[0], image_size[1], 3))
    elif base_model_type == "Inception_v3":
        base_model = tf.keras.applications.InceptionV3(include_top=False, weights="imagenet", pooling="avg",
                                                       input_shape=(image_size[0], image_size[1], 3))
    elif base_model_type == "NASNetLarge":
        base_model = tf.keras.applications.NASNetLarge(include_top=False, weights="imagenet", pooling="avg",
                                                       input_shape=(image_size[0], image_size[1], 3))
    elif base_model_type == "MobileNetV3Small":
        base_model = tf.keras.applications.MobileNetV3Small(include_top=False, weights="imagenet", pooling="avg",
                                                            input_shape=(image_size[0], image_size[1], 3))
    else:
        raise ValueError(f"Model of type '{base_model_type}' is not supported.")
    model = tf.keras.models.Sequential()
    model.add(base_model)
    model.add(tf.keras.layers.Dense(2, activation="softmax"))

    return model


def train_and_eval(images, df_labels, image_size, num_augmentations, use_tta, batch_size, init_lr, lr_decay_base):
    keras_roc_aucs = []
    acc_means = []
    for datetime_of_test_set in config["DATETIME_OF_TEST_SET"]:
        # Train/test split:
        x_train, y_train, x_test, y_test, x_val, y_val = train_test_split(
            images, df_labels, datetime_of_test_set, datetime_of_test_set + pd.DateOffset(
                days=config["NUM_DAYS_OF_TRAIN_AND_TEST_SET"]),
            test_and_val_days=config["NUM_DAYS_OF_TRAIN_AND_TEST_SET"])

        # Augment images:
        x_train, y_train = augment(x_train, y_train, config["MIN_AUG_SHIFT"], config["MAX_AUG_SHIFT"],
                                   config["MIN_BRIGHTNESS_INCREASE"],
                                   config["MAX_BRIGHTNESS_INCREASE"], config["USE_LR_FLIP"], num_augmentations)

        # Build model:
        if config["USE_MULTI_GPU"]:
            strategy = tf.distribute.MirroredStrategy(devices=["/gpu:0", "/gpu:1"])
            with strategy.scope():
                model = build_model(config["BASE_MODEL_TYPE"], image_size)
                model = tf.keras.utils.multi_gpu_model(model, gpus=2)
        else:
            model = build_model(config["BASE_MODEL_TYPE"], image_size)

        # Train:
        early_stopper = tf.keras.callbacks.EarlyStopping(monitor="val_loss", patience=4)
        checkpoint = tf.keras.callbacks.ModelCheckpoint(filepath="models/weights.hdf5", verbose=1, save_best_only=True)
        learning_rate_scheduler = tf.keras.callbacks.LearningRateScheduler(
            lambda epoch: init_lr * (lr_decay_base ** np.floor(epoch / 2)))
        model.compile(optimizer=tf.keras.optimizers.Adam(learning_rate=init_lr), loss="mse",
                      metrics=[tf.keras.metrics.AUC(num_thresholds=2 ** 16), "accuracy"])
        if config["MODEL_TO_LOAD"] is None:
            model.fit(x_train, y_train, validation_data=(x_test, y_test), epochs=config["EPOCHS"],
                      batch_size=batch_size,
                      callbacks=[early_stopper, checkpoint, learning_rate_scheduler])
            model.load_weights("models/weights.hdf5")
        else:
            model = tf.keras.models.load_model(os.path.join(os.path.dirname(os.path.abspath(__file__)), "08_models",
                                                            config["MODEL_TO_LOAD"]))
            if config["FIT_LOADED_MODEL"]:
                model.fit(x_train, y_train, validation_data=(x_test, y_test), epochs=config["EPOCHS"],
                          batch_size=config["BATCH_SIZE"],
                          callbacks=[early_stopper, checkpoint, learning_rate_scheduler])
                model.load_weights("models/weights.hdf5")

        # Save model:
        if config["MODEL_TO_LOAD"] is None or config["FIT_LOADED_MODEL"]:
            model.save(os.path.join(os.path.dirname(os.path.abspath(__file__)), generate_file_name_of_model(
                num_augmentations, use_tta, batch_size, image_size)))

        # Evaluate:
        predictions = []
        for i in range(x_val.shape[0]):
            # shift = max(1, int(np.round(image_size[0] * 0.015, 0)))
            if use_tta:
                current_predictions = model.predict(np.reshape(np.array([
                    x_val[i], np.fliplr(x_val[i]), increase_brightness(x_val[i], 50)]),
                    (-1, image_size[0], image_size[1], 3)))
            else:
                current_predictions = model.predict(np.reshape(np.array([x_val[i]]),
                                                               (1, image_size[0], image_size[1], 3)))
            predictions.append([np.mean(current_predictions, axis=0)])
        # roc_auc_means.append(
        #    sklearn.metrics.roc_auc_score([int(np.round(p[0][0])) for p in predictions], [y[0] for y in y_val]))
        acc_means.append(
            sklearn.metrics.accuracy_score([int(np.round(p[0][0])) for p in predictions], [y[0] for y in y_val]))
        keras_roc_aucs.append(model.evaluate(x_val, y_val)[1])

        if config["SAVE_WRONG_CLASSIFIED_IMAGES"]:
            predictions = model.predict(x_val)
            for i in range(len(predictions)):
                if int(np.round(predictions[i][0])) != y_val[i][0]:
                    cv2.imwrite(join(dirname(abspath(__file__)), "non_detected_images", str(i) + ".jpg"), x_val[i])

        del model, x_train, y_train, x_test, y_test, x_val, y_val

    return np.mean(keras_roc_aucs), np.mean(acc_means)


def train_eval_and_write_results(images, df_labels, image_size, num_augmentations, use_tta, batch_size, init_lr,
                                 lr_decay_base):
    keras_roc_auc, acc_mean = train_and_eval(images, df_labels, image_size, num_augmentations,
                                             use_tta, batch_size, init_lr, lr_decay_base)
    model_to_load = config["MODEL_TO_LOAD"]
    append_row_to_eval_df(f"eval/{model_to_load}.csv", {
        "timestamp": datetime.now().strftime("%Y-%m-%dT%H:%M:%S"),
        "image_size": f"{image_size[0]}x{image_size[1]}",
        "num_augmentations": num_augmentations,
        "use_tta": use_tta,
        "batch_size": batch_size,
        "init_lr": init_lr,
        "lr_decay_base": lr_decay_base,
        "keras_roc_auc": keras_roc_auc,
        "acc_mean": acc_mean})
    return acc_mean
