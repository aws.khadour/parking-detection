import os
import re
from multiprocessing import Pool
import configration
import cv2
import numpy as np
import pandas as pd







def increase_brightness(img, value):
    hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
    h, s, v = cv2.split(hsv)

    lim = 255 - value
    v[v > lim] = 255
    v[v <= lim] += np.uint8(value)

    final_hsv = cv2.merge((h, s, v))
    img = cv2.cvtColor(final_hsv, cv2.COLOR_HSV2BGR)
    return img


def augment(images, labels, min_shift=0, max_shift=0, min_brightness_increase=0, use_lr_flip=False,
            max_brightness_increase=0, num_augmentations=0):
    if num_augmentations <= 0:
        return images, labels

    augmented_images = []
    new_labels = []
    for i in range(images.shape[0]):
        # Add non augmented version:
        augmented_images.append([images[i]])
        new_labels.append([labels[i]])

        # Add augmented versions:
        for j in range(num_augmentations):
            augmented_image = images[i]
            new_label = labels[i]

            # Mix with random image (mix-up augmentation):
            delta = np.random.beta(a=0.4, b=0.4)
            rand_idx = np.random.choice(images.shape[0])
            augmented_image = np.uint8(np.round(delta * augmented_image + (1 - delta) * images[rand_idx], 0))
            new_label = delta * new_label + (1 - delta) * labels[rand_idx]

            # Flip image:
            if use_lr_flip and np.random.choice([True, False]):
                augmented_image = np.fliplr(augmented_image)

            # Horizontal and vertical shift:
            shift = np.random.choice(np.arange(min_shift, max_shift + 1)) * np.random.choice([-1, 1])
            augmented_image = np.roll(augmented_image, shift, axis=0)
            shift = np.random.choice(np.arange(min_shift, max_shift + 1)) * np.random.choice([-1, 1])
            augmented_image = np.roll(augmented_image, shift, axis=1)

            # Calculate increase or decrease of brightness:
            brightness_increase = np.random.choice([min_brightness_increase, max_brightness_increase + 1])

            # Append image and label to list:
            augmented_images.append([increase_brightness(augmented_image, brightness_increase)])
            new_labels.append([new_label])
    augmented_images = np.concatenate(augmented_images)
    new_labels = np.concatenate(new_labels)

    return augmented_images, new_labels


def _read_images(image_names, sub_dir, image_dir_path, image_size):
    images = None
    for image_name in image_names:
        image = cv2.imread(image_dir_path + sub_dir + "/" + image_name)
        image = cv2.resize(image, image_size)
        if images is None:
            images = [image]
        else:
            images = np.concatenate([images, [image]])

    return images


def _get_attributes_for_read_image_pool(image_names, sub_dir, image_dir_path, image_size, num_cpus):
    n = len(image_names)
    image_name_bins = [image_names[i:i + int(n / num_cpus)] for i in range(0, n, int(n / num_cpus))]
    # return [list(zip(image_name_bin, [sub_dir] * n, [image_dir_path] * n, [image_size] * n))
    #         for image_name_bin in image_name_bins]
    return list(zip(image_name_bins, [sub_dir] * n, [image_dir_path] * n, [image_size] * n))


def read_images(image_dir_path, num_cpus, image_size):
    sub_dirs = os.listdir(image_dir_path)
    images = []
    image_names = []
    for sub_dir in sub_dirs:
        current_image_names = os.listdir(image_dir_path + sub_dir)
        # sub_dir_and_image_names = [sub_dir + "/" + image_name for image_name in current_image_names]
        with Pool(num_cpus) as p:
            current_images = p.starmap(_read_images, _get_attributes_for_read_image_pool(
                current_image_names, sub_dir, image_dir_path, image_size, num_cpus))
        images += current_images
        image_names += current_image_names
    images = np.concatenate(images)
    idx_sorted = np.argsort(image_names)
    image_names = np.array(image_names)[idx_sorted]
    images = images[idx_sorted]

    return images, image_names


def read_labels(labels_dir_path):
    labels_files = os.listdir(labels_dir_path)
    dfs = []
    for labels_file in labels_files:
        dfs.append(pd.read_csv(labels_dir_path + labels_file))
    df = pd.concat(dfs, axis=0).sort_values("img")

    # To datetime:
    pattern = "T[0-9][0-9]-[0-9][0-9]"
    df["datetime"] = pd.to_datetime(
        df["img"].apply(lambda x: re.sub(pattern, re.search(pattern, x).group(0).replace("-", ":"), x).split("_")[0]),
        infer_datetime_format=True)

    # To numeric:
    df["empty"] = df["empty"].astype(np.int8)
    df["occupied"] = df["occupied"].astype(np.int8)
    df["unknown"] = df["unknown"].astype(np.int8)

    # Remove samples with 'unknown' label:
    df = df[df.unknown == 0]

    return df


def _get_data_not_in_date_range(df, date_from, date_to):
    return df[(df["datetime"].dt.date >= date_from) & (df["datetime"].dt.date < date_to)]


def _date_is_smaller_or_time_is_larger(df, ts):
    return (df["datetime"] >= ts) | (df["datetime"].dt.date < ts.date())


def _date_is_smaller_or_larger(df, ts):
    return (df["datetime"].date() > ts) | (df["datetime"].dt.date < ts.date())


def _remove_images_that_are_similar_to_train_set(x_train, y_train, x_test, y_test, x_val, y_val):
    min_difference = 0.03
    y_test_idx_to_keep = []
    y_val_idx_to_keep = []

    # TODO: Unfinished! Implement for validation data.
    for i in range(x_test.shape[0]):
        if y_test[i][1] == 1:
            similar_image_found = False
            for j in range(x_train.shape[0]):
                if y_train[j][1] == 1:
                    print(cv2.matchTemplate(x_train[j], x_test[i], cv2.TM_SQDIFF_NORMED)[0][0])
                    if cv2.matchTemplate(x_train[j], x_test[i], cv2.TM_SQDIFF_NORMED)[0][0] < min_difference:
                        similar_image_found = True
                        break
            if not similar_image_found:
                y_test_idx_to_keep.append(i)
        else:
            y_test_idx_to_keep.append(i)
    y_test_idx_to_keep = np.array(y_test_idx_to_keep)

    return x_test[y_test_idx_to_keep], y_test[y_test_idx_to_keep], x_val[y_val_idx_to_keep], y_val[y_val_idx_to_keep]


def train_test_split(images, df_labels, datetime_of_test_set, datetime_of_validation_set, test_and_val_days=1):
    # Sort and reset index of labels:
    df_labels = df_labels.sort_values(["datetime", "img"])
    df_labels.index = np.arange(df_labels.shape[0])

    min_test_dt = datetime_of_test_set
    max_test_dt = datetime_of_test_set + pd.DateOffset(days=test_and_val_days)
    min_val_dt = datetime_of_validation_set
    max_val_dt = datetime_of_validation_set + pd.DateOffset(days=test_and_val_days)
    y_train = df_labels[(df_labels["datetime"].dt.date < (min_test_dt.date() - pd.DateOffset(days=1)).date()) &
                        (df_labels["datetime"].dt.date < (min_val_dt.date() - pd.DateOffset(days=1)).date()) |
                        (df_labels["datetime"].dt.date >= (max_test_dt.date() + pd.DateOffset(days=1)).date()) &
                        (df_labels["datetime"].dt.date >= (max_val_dt.date() + pd.DateOffset(days=1)).date())][
        ["empty", "occupied"]]
    y_test = df_labels[(df_labels["datetime"] >= min_test_dt) &
                       (df_labels["datetime"] < max_test_dt)][["empty", "occupied"]]
    y_val = df_labels[(df_labels["datetime"] >= min_val_dt) &
                      (df_labels["datetime"] < max_val_dt)][["empty", "occupied"]]
    x_train = images[y_train.index]
    x_test = images[y_test.index]
    x_val = images[y_val.index]
    y_train = np.array(y_train)
    y_test = np.array(y_test)
    y_val = np.array(y_val)

    return x_train, y_train, x_test, y_test, x_val, y_val


def get_validation_x_and_y(image_dir_path, min_width_of_images_to_read,config):
    #Read the Configrations from Json File
    num_days_of_validation_and_test_set = config["NUM_DAYS_OF_TRAIN_AND_TEST_SET"]
    datetime_of_test_set = config["DATETIME_OF_TEST_SET"]
    datetime_of_validation_data_set = datetime_of_test_set + pd.DateOffset(num_days_of_validation_and_test_set)
    min_val_dt = datetime_of_validation_data_set
    max_val_dt = datetime_of_validation_data_set + pd.DateOffset(days=num_days_of_validation_and_test_set)

    # Read data:
    labels_dir_path = config["LABELS_DIR_PATH"]
    for image_size in config["IMAGE_SIZE"]:

        df_labels = read_labels(labels_dir_path)
        df_labels = df_labels[(df_labels["datetime"] >= min_val_dt) &
                          (df_labels["datetime"] < max_val_dt)]
        img_Name = df_labels["img"].to_numpy()
        images, images_found = read_images_for_labels(image_dir_path, img_Name, image_size,
                                                  min_width_of_images_to_read)

    # Remove labels for images that were not found:
        df_labels["image_found"] = images_found
        df_labels = df_labels[df_labels["image_found"]]
        del df_labels["image_found"]


    y_val = df_labels[["empty", "occupied"]].to_numpy()
    x_val = images

    return x_val, y_val



def read_images_for_labels(image_dir_path, file_names, image_size, min_width_of_images_to_read):
    images_found = []
    images = None
    for file_name in file_names:
        sub_dir = file_name.split("T")[0][:-3]
        image = cv2.imread(image_dir_path + sub_dir + "/" + file_name)
        if image is None:
            images_found.append(False)
            continue
        if images is None and image.shape[1] >= min_width_of_images_to_read:
            image = cv2.resize(image, image_size)
            images = [image]
            images_found.append(True)
        elif image.shape[0] >= min_width_of_images_to_read:
            image = cv2.resize(image, image_size)
            images = np.concatenate([images, [image]])
            images_found.append(True)
        else:
            images_found.append(False)
    return images, images_found