import tensorflow as tf
import os
import numpy as np
from configration import get
from converter import convert_and_save_model_lite,create_interpreter,convert_and_quantize_and_save_mode_lite

# get the Json  configration  file
config = get()

if __name__ == "__main__":
    np.random.seed(config["SEED"])
    temp = os.path.join(os.path.dirname(os.path.abspath(__file__)))

    # for image_size in config["IMAGE_SIZE"]:
    # Read data:
    # df_labels = read_labels(config["LABELS_DIR_PATH"])
    # images, images_found = read_images_for_labels(config["IMAGE_DIR_PATH"], np.array(df_labels["img"]), image_size,
    #     config["MIN_WIDTH_OF_IMAGES_TO_READ"])

    # Remove labels for images that were not found:
    # df_labels["image_found"] = images_found
    # df_labels = df_labels[df_labels["image_found"]]
    # del df_labels["image_found"]

    # Train and evaluate:
    # if config["NUM_RANDOM_SEARCH_SAMPLES"] > 0:
    # Random search:
    # for i in range(config["NUM_RANDOM_SEARCH_SAMPLES"]):
    #   num_augmentations = np.random.choice(np.arange(min(config["NUM_AUGMENTATIONS"]), max(config["NUM_AUGMENTATIONS"]) + 1))
    #  use_tta = np.random.choice(np.arange(int(min(config["USE_TTA"])), int(max(config["USE_TTA"])) + 1)) == 1
    #  batch_size = np.random.choice(np.arange(min(config["BATCH_SIZE"]), max(config["BATCH_SIZE"]) + 1))
    # init_lr = np.random.uniform(min(config["INIT_LR"]), max(config["INIT_LR"]))
    # lr_decay_base = np.random.uniform(min(config["LR_DECAY_BASE"]), max(config["LR_DECAY_BASE"]))
    # train_eval_and_write_results(images, df_labels, image_size, num_augmentations, use_tta,
    #                   batch_size, init_lr, lr_decay_base)
    # elif config["NUM_BAYES_SEARCH_SAMPLES"] > 0:
    # Bayes search:
    #   def objective(trial):
    #      return train_eval_and_write_results(
    #         images, df_labels, image_size,
    #        trial.suggest_int("num_augmentations", min(config["NUM_AUGMENTATIONS"]), max(config["NUM_AUGMENTATIONS"])),
    #       trial.suggest_categorical("use_tta", [min(config["USE_TTA"]), max(config["USE_TTA"])]),
    #      trial.suggest_int("batch_size", min(config["BATCH_SIZE"]), max(config["BATCH_SIZE"])),
    #     trial.suggest_uniform("init_lr", min(config["INIT_LR"]), max(config["INIT_LR"])),
    #    trial.suggest_uniform("lr_decay_base", min(config["LR_DECAY_BASE"]), max(config["LR_DECAY_BASE"])))

    # study = optuna.create_study(direction="maximize")
    # study.optimize(objective, n_trials=config["NUM_BAYES_SEARCH_SAMPLES"])
    # else:
    # Grid search:
    #   for num_augmentations in config["NUM_AUGMENTATIONS"]:
    #      for use_tta in config["USE_TTA"]:
    #         for batch_size in config["BATCH_SIZE"]:
    #            for init_lr in config["INIT_LR"]:
    #               for lr_decay_base in config["LR_DECAY_BASE"]:
    #                   train_eval_and_write_results(images, df_labels, image_size, num_augmentations, use_tta,
    #                                                batch_size, init_lr, lr_decay_base)

    #
    model = tf.keras.models.load_model(config["MODEL_TO_LOAD"])
    tflite_model = convert_and_save_model_lite(model)


