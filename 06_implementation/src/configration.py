import json
import pandas as pd
import numpy as np

# Module to easily load the configuration and if not found return a default.

default_configuration =   {
    "LABEL_STYLE" : ["square", "rect"][0],
    "IMAGE_DIR_PATH":f"C:/Users/AWS/Desktop/AIOT-MASTER-Project/Rastanlagenbilder/cropped_square/2021/",
    "LABELS_DIR_PATH": "C:/Users/AWS/Desktop/AIOT-MASTER-Project/labels/labels/labels/cropped/",
    "DATETIME_OF_TEST_SET": pd.to_datetime("2021-03-01 00:00"),
    "NUM_DAYS_OF_TRAIN_AND_TEST_SET": 2,
    "IMAGE_SIZE":[(75, 75)],
    "EPOCHS": 30,
    "BATCH_SIZE" :[230],
    "INIT_LR" : [0.0001, 0.01],
    "MODEL_TO_LOAD" : "C:/Users/AWS/Desktop/AIOT-MASTER-Project/labels/labels/labels/cropped/",
    "FIT_LOADED_MODEL" : False,
    "NUM_RANDOM_SEARCH_SAMPLES" : 0,
    "NUM_BAYES_SEARCH_SAMPLES" : 2,
    "USE_MULTI_GPU" : False,
    "SAVE_WRONG_CLASSIFIED_IMAGES" : False,
    "SEED" : 2877,
    "MIN_WIDTH_OF_IMAGES_TO_READ" : 50,
    "MODEL_LITE_NAME" : "09_lite_models/un-quant/Mobilenetsmall.tflite",
    "MODEL_LITE_NAME_QUANTIZED" : "09_lite_models/quant/Mobilnetsmall_quant.tflite",
    "BASE_MODEL_TYPE" : ["DenseNet121", "InceptionResNetV2", "Xception" , "ResNet152V2.h5","DenseNet169.h5"][1],
    "MIN_AUG_SHIFT" : (int(np.round(0.015 * 75, 0)), int(np.round(0.04 * 75, 0))),
    "MAX_AUG_SHIFT" : (int(np.round(0.015 * 75, 0)), int(np.round(0.04 * 75, 0))),
    "MIN_BRIGHTNESS_INCREASE" : (0, 50),
    "MAX_BRIGHTNESS_INCREASE" : (0, 50),
    "USE_LR_FLIP": True,
    "NUM_AUGMENTATIONS" : [0, 20],
    "LR_DECAY_BASE" : [0.5, 0.95],
    "USE_TTA" : [False, True]

}

def get():
    try:
        with open("config.json", "r") as file:
            config = json.loads(file.read())
            #print("Using configuration from " + file.name + ".")
            return config
    except Exception:
        print("No configuration found, using default configuration.")
        return default_configuration
