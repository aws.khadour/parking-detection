import os
import numpy as np
import sklearn.metrics
import tensorflow as tf
from dataPreprocessing import get_validation_x_and_y

#
config = configration.get()
image_path = config["IMAGE_DIR_PATH"]
min_width_of_images_to_read = config["MIN_WIDTH_OF_IMAGES_TO_READ"]
x_val_square, y_val_square = get_validation_x_and_y(image_path, min_width_of_images_to_read)

# this Method is used to evaluate Model with Validation data
# this Evaluation happen after the Training and Testing with Train and Test data-set

def evaluate_tenserflow_model():
    # Get validation data:
    x_val_square, y_val_square = get_validation_x_and_y(image_path, min_width_of_images_to_read)

    accuracy = 0
    roc_auc = 0
    predictions = []
    model = tf.keras.models.load_model(os.path.join(os.path.dirname(os.path.abspath(__file__)), "models",
                                                    config["MODEL_TO_LOAD"]))

    predictions.append(model.predict(x_val_square))
    prediction = np.array([predictions[np.argmax([p[i][1] for p in predictions])][i]
                           for i in range(predictions[0].shape[0])])
    accuracy += sklearn.metrics.accuracy_score([int(np.round(p[0])) for p in prediction],
                                               [y[0] for y in y_val_square])
    roc_auc += sklearn.metrics.roc_auc_score([int(np.round(p[0])) for p in prediction],
                                            [y[0] for y in y_val_square])

    accuracy /= len(config["MODELS_TO_LOAD"])
    roc_auc /= len(config["MODELS_TO_LOAD"])
    print(f"ROC AUC:  {np.round(roc_auc, 3)}")
    print(f"Accuracy: {np.round(accuracy, 3)}")



# this Method is used to predict the class of the output of lite_model for the validation data_set
# the class can be either 0 "empty" or 1 "occupied"
# prediction_digits will be processed later to extract the accuracy of the validation data_set
# the Evaluation depend on the type of Quantization :
 # F -> floating point 32 bit
 # U -> Uint 8 bit
 # I -> Int 8 bit


def evaluate_model_lite(interpreter, type_of_quantization):
    input_index = interpreter.get_input_details()[0]["index"]
    output_index = interpreter.get_output_details()[0]["index"]

    # Run predictions on every image in the "test" dataset.
    prediction_digits = []
    for i, x_val_square_i in enumerate(x_val_square):
        if i % 1000 == 0:
            print('Evaluated on {n} results so far.'.format(n=i))
        # Pre-processing: add batch dimension and convert to float32 to match with
        # the model's input data format.
        if type_of_quantization == "F":
            x_val_square_i = np.expand_dims(x_val_square_i, axis=0).astype(np.float32)
        elif type_of_quantization == "U":
            x_val_square_i = np.expand_dims(x_val_square_i, axis=0).astype(np.uint8)
        elif type_of_quantization == "I":
            x_val_square_i = np.expand_dims(x_val_square_i, axis=0).astype(np.int8)

    interpreter.set_tensor(input_index, x_val_square_i)

    # Run inference.
    interpreter.invoke()

    # Post-processing: remove batch dimension and find the digit with highest
    # probability.
    output = interpreter.tensor(output_index)
    digit = np.argmax(output()[0])
    prediction_digits.append(digit)

    print('\n')

    prediction_digits = np.array(prediction_digits)

    return prediction_digits


# calculate accuracy depending on predicted classes from "evaluate_model_lite" Method
def calculate_accuracy_for_lite_model(interpreter):
    # this list contains classes-Numbers (0->Empty) and (1->occupied)
    # the reason for that , returned list from evaluate_methods is the Predicted Classes-numbers
    # and the labeled data is indexed to read as  following :
    # first column for empty places, and when the row of the Column Img-name is 1 for the empty Column, that means the image is empty
    # that happen same for second column for occupied state
    # finally the label values (ground truth) 1 that refer to the Column not for the Class-Number
    test_result_for_compare = []
    prediction_digits = evaluate_model_lite(interpreter,type_of_quantization="F")


    for x in (y_val_square):
     if (x[0] == 1):
        test_result_for_compare.append(0)
     if (x[1] == 1):
        test_result_for_compare.append(1)

    test_result_for_compare = np.array(test_result_for_compare)
    accuracy = (prediction_digits == test_result_for_compare).mean()
    print(accuracy)
    print(prediction_digits)

    return accuracy
